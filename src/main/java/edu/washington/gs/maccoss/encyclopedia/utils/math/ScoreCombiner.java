package edu.washington.gs.maccoss.encyclopedia.utils.math;

public interface ScoreCombiner {
	public float getScore(float[] data);
}
